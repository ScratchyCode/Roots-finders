# Function-roots

Search the roots of an arbitrary function in a given numeric range with two different numeric methods.

# How to execute

Compile the tinyexpr library with:

gcc -c tinyexpr.c -lm

Then, if you want to use the Bisection method, compile Bisection.c with:

gcc tinyexpr.o Bisection.c -lm

else, if you want to use Newton's method, compile Tangents.c with:

gcc tinyexpr.o Tangents.c -lm

# ATTENTION

Each method has its weaknesses.
For example the bisection method can not be applied if the hypotheses of the Bolzano's theorem are not valid.
However with the Newton's method the convergence at the root is not guaranteed, in particular when the first derivative of the function varies considerably near the root.
