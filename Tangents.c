// Coded by ScratchyCode
// Newton's method
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "tinyexpr.h"

#define DIM 10000
#define PRECISION 0.000000001

int main(){
    double a=0, b=0, approx=0, x=0;
    double diff1=0, diff2=0, num=0, denom=0;
    double resolution=0;
    double min=1E12;
    te_variable vars[] = {{"x",&x}};
    char expression[DIM];
    
    printf("\nEnter the analytical expression of the function: ");
    fgets(expression,DIM,stdin);
    expression[strlen(expression) - 1] = 0;
    
    printf("Enter the minimum of the interval (INF): ");
    scanf("%lf",&a);
    
    printf("Enter the maximum of the interval (SUP): ");
    scanf("%lf",&b);
    
    if(a >= b){
        printf("\nError: the INF value must be less than SUP value!\n");
        exit(0);
    }
    
    printf("Enter the root resolution: ");
    scanf("%lf",&resolution);
    
    if(resolution <= 0){
        printf("\nError: the resolution must be greater than 0!\n");
        exit(0);
    }
    
    int err;
    te_expr *n = te_compile(expression,vars,1,&err);
    
    x = approx = a;
    if(n){
        while(fabs(min) > resolution){
            x = approx;
            num = te_eval(n);
            diff2 = te_eval(n);
            x += PRECISION;
            diff1 = te_eval(n);
            denom = (diff1 - diff2) / PRECISION;
            x -= PRECISION;
            approx = x - (num/denom);
            x += resolution;
            
            min = te_eval(n);
        }
    }else{
        printf("\t%*s^\nError",err-1,"");
        exit(1);
    }
    
    printf("\nApproximate root: x = %lf\n\n",approx);
    
    te_free(n);

    return 0;
}
