// Coded by ScratchyCode
// Bisection method
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "tinyexpr.h"

#define DIM 10000

int main(){
    double a=0, b=0, c=0, p=0, x=0, Fa=0, Fc=0;
    double precision=0;
    te_variable vars[] = {{"x",&x}};
    char expression[DIM];
    
    printf("\nEnter the analytical expression of the function: ");
    fgets(expression,DIM,stdin);
    expression[strlen(expression) - 1] = 0;
    
    printf("Enter the minimum of the interval (INF): ");
    scanf("%lf",&a);
    
    printf("Enter the maximum of the interval (SUP): ");
    scanf("%lf",&b);
    
    if(a >= b){
        printf("\nError: the INF value must be less than SUP value!\n");
        exit(0);
    }
    
    printf("Enter the root resolution: ");
    scanf("%lf",&precision);
    
    if(precision <= 0){
        printf("\nError: the precision must be greater than 0!\n");
        exit(0);
    }
    
    double delta = fabs(a - b);
    
    int err;
    te_expr *n = te_compile(expression,vars,1,&err);
    
    if(n){
        while(delta > precision){
            c = (a + b) / 2;
            x = a;
            Fa = te_eval(n);
            x = c;
            Fc = te_eval(n);
            p = Fa * Fc;
        
            if(p > 0){
                a = c;
            }else if(p < 0){
                b = c;
            }else{
                // exact solution --> delta == 0 --> exit
                a = b = c;
            }
        
            delta = fabs(a - b);
        }
    }else{
        printf("\t%*s^\nError",err-1,"");
        exit(1);
    }
    
    printf("\nApproximate root: x = %lf\n\n",c);
    
    te_free(n);

    return 0;
}
